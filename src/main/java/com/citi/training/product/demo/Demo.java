package com.citi.training.product.demo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.citi.training.product.model.Product;
import com.citi.training.product.repository.ProductRepository;

@Component
public class Demo implements ApplicationRunner {

    @Autowired
    private ProductRepository productRepository;

    public void run(ApplicationArguments appArgs) {
        System.out.println("Creating some products");
        String[] productNames = {"Beans", "Jam", "Ham"};
        Random random = new Random();

        for(int i=0; i<productNames.length; ++i) {
            Product thisProduct = new Product(i,
                                              productNames[i],
                                              roundTwoPlaces(random.nextDouble()));

            System.out.println("Created Product: " + thisProduct);

            productRepository.saveProduct(thisProduct);
        }

        System.out.println("\nAll Products:");
        for(Product product: productRepository.getAllProducts()) {
            System.out.println(product);
        }
  
        System.out.println("Demo Finished - Exiting");
    }

    private double roundTwoPlaces(double value) {
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
