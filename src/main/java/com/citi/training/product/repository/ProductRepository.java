package com.citi.training.product.repository;

import java.util.List;

import com.citi.training.product.model.Product;

public interface ProductRepository {

    void saveProduct(Product product);

    Product getProduct(int id);

    List<Product> getAllProducts();
}
