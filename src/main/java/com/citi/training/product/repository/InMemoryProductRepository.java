package com.citi.training.product.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.citi.training.product.model.Product;

@Component
public class InMemoryProductRepository implements ProductRepository{

    private Map<Integer, Product> allProducts = new HashMap<Integer, Product>();

    public void saveProduct(Product product) {
        allProducts.put(product.getId(), product);
    }

    public Product getProduct(int id) {
        return allProducts.get(id);
    }

    public List<Product> getAllProducts() {
        return new ArrayList<Product>(allProducts.values());
    }

}
