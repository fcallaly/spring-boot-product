package com.citi.training.product.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class ProductTests {

    private int testId = 23;
    private String testName = "Spring Beans";
    private double testPrice = 67.87;

    @Test
    public void test_Product_constructor() {
        Product testProduct = new Product(testId, testName, testPrice);;

        assertEquals(testId, testProduct.getId());
        assertEquals(testName, testProduct.getName());
        assertEquals(testPrice, testProduct.getPrice(), 0.0001);
    }

    @Test
    public void test_Product_toString() {
        String testString = new Product(testId, testName, testPrice).toString();

        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testName));
        assertTrue(testString.contains((new Double(testPrice).toString())));
    }
}
