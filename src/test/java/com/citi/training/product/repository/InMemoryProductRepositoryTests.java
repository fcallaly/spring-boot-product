package com.citi.training.product.repository;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citi.training.product.model.Product;

public class InMemoryProductRepositoryTests {

    private static Logger LOG = LoggerFactory.getLogger(InMemoryProductRepositoryTests.class);

    private int testId = 879;
    private String testName = "A Product for testing!";
    private double testPrice = 87.44449;

    @Test
    public void test_saveAndGetProduct() {
        Product testProduct = new Product(testId, testName, testPrice);
        InMemoryProductRepository testRepo = new InMemoryProductRepository();

        testRepo.saveProduct(testProduct);
 
        assertTrue(testRepo.getProduct(testId).equals(testProduct));

    }

    @Test
    public void test_saveAndGetAllProducts() {
        Product[] testProductArray = new Product[100];
        InMemoryProductRepository testRepo = new InMemoryProductRepository();

        for(int i=0; i<testProductArray.length; ++i) {
            testProductArray[i] = new Product(testId + i, testName, testPrice + i);

            testRepo.saveProduct(testProductArray[i]);
        }

        List<Product> returnedProducts = testRepo.getAllProducts();
        LOG.info("Received [" + returnedProducts.size() +
                 "] products from repository");

        for(Product thisProduct: testProductArray) {
            assertTrue(returnedProducts.contains(thisProduct));
        }
        LOG.info("Matched [" + testProductArray.length + "] products");
    }
}
